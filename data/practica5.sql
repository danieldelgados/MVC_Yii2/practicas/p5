DROP DATABASE IF EXISTS practica5;
CREATE DATABASE practica5;
USE practica5;

CREATE TABLE noticias(
  id int AUTO_INCREMENT,
  titulo varchar(255),
  texto text,
  categoria varchar(255),
  PRIMARY KEY(id)
  );

INSERT noticias ( titulo, texto,categoria)
  VALUES 
  ('Titulo de la noticia 1','Texto de la noticia 1','economia'),
  ('Titulo de la noticia 2','Texto de la noticia 2','deporte'),
  ('Titulo de la noticia 3','Texto de la noticia 3','deporte'),
  ('Titulo de la noticia 4','Texto de la noticia 4','politica'),
  ('Titulo de la noticia 5','Texto de la noticia 5','politica'),
  ('Titulo de la noticia 6','Texto de la noticia 6','politica');